import SocketServer
import json
import db_connect
import learn_you_machine

dat_obtained_from_lu={}
data_flag=0

#The ip address
IP_ADD= '127.0.0.1'
#The port number
PORT= 5001

class MyTCPServer(SocketServer.ThreadingTCPServer):
    allow_reuse_address = True

class MyTCPServerHandler(SocketServer.BaseRequestHandler):
  def handle(self):
      try:
          dat_obtained_from_lu = json.loads(self.request.recv(PORT).strip())
          self.request.sendall(json.dumps({'returns':'ok'}))
          the_rss_stuff = db_connect.get_stuff_from_db(dat_obtained_from_lu)
          self.request.sendall(json.dumps(the_rss_stuff))
          learn_da = json.loads(self.request.recv(PORT).strip())
          learn_you_machine.mach(dat_obtained_from_lu, learn_da)
      except Exception:
          self.request.sendall(json.dumps({'hello': 'Hi'}))

server = MyTCPServer((IP_ADD,PORT), MyTCPServerHandler)
server.serve_forever()