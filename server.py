import SocketServer
import json

data={}
data_flag=0
class MyTCPServer(SocketServer.ThreadingTCPServer):
    allow_reuse_address = True

class MyTCPServerHandler(SocketServer.BaseRequestHandler):
  def handle(self):
      try:
          data = json.loads(self.request.recv(1024).strip())
          self.request.sendall(json.dumps({'return':'ok'}))
          data_flag = 1
          
      except Exception, e:
          print "Exception wile receiving message: ", e

def make_server( ip_add, port):              
  server = MyTCPServer((ip_add, port), MyTCPServerHandler)
  server.serve_forever()
